

const { requestApi } = require('../helpers/requestApi');
exports.getCharacters = async (req, res) => {

   return await requestApi('https://rickandmortyapi.com/api/character', 'get'); 
};