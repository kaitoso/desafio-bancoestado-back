# Desafío bancoestado

Servicio rest que consume el api de rick y morty con axios, y devuelve el listado de la primera pagina de personajes para la postulación al cargo de desarrollador en bancoestado
## Installacion

version de node 14,ejecute el siguiente comando en la ruta raiz para instalar las dependencias

```bash
npm install
```

## Ejecutar
Para ejecutar el proyecto correr el siguiente comando:
```bash
node index.js
```
El servicio rest correrá en el puerto 8000
## Documentación

para ver la documentacion del endpoint entrar en http://localhost:8000/api-docs
