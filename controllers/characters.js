
const {getCharacters} = require('../services/characters');
exports.getCharacters = async (req, res) => {
    
    const response = await getCharacters();
    if (response.error) {
        return res.status(400).json(response);
    }
    const characters = {characters: [...response.results]};
    res.json(characters);


};
