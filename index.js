// generic imports
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");


const characterRoutes = require("./routes/characters");

// app - express
const app = express();


// middlewares
app.use(bodyParser.json());
app.use(cors());

// Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            version: "1.0.0",
            title: "Desafio bancoestado",
            description: "api para postulacion a puesto de desarrollador en bancoestado",
            contact: {
                name: "CarlosValenzuela"
            },
            servers: ["http://localhost:8000"]
        }
    },
    apis: ['./routes/*.js']
};


const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/*routes middlewares*/
app.use("/api", characterRoutes );

const port = 8000;

app.listen(port, () => {
    console.log(`Servidor corriendo en el puerto ${port}`);
});