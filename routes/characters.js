const express = require("express");
const router = express.Router();

const {
   getCharacters
} = require("../controllers/characters");



router.get("/characters", getCharacters);

/**
 * @swagger   
 * /api/characters: 
 *  get:
 *    summary: characters
 *    description: retorna characters desde el api de rick y morty
 *    requestBody: 
 *      content:
 *        application/json:
 *              
 *    responses:
 *      "200":
 *         description: respuesta satisfactoria
 *      "400":
 *         description: peticion fallida
 */


module.exports = router;